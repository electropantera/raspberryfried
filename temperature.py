#!/usr/bin/env python
import time
import datetime
from subprocess import check_output
import logging
logging.basicConfig(filename='temperature.log',level=logging.DEBUG)

while True:
        temp = check_output(["/opt/vc/bin/vcgencmd", "measure_temp"])
        dt = datetime.datetime.now()
        message = "{},{}".format(dt,temp)
        logging.info(message)
        print(message)
        time.sleep(30)
